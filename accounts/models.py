from django.db import models


class User(models.Model):
    first_name = models.CharField(max_length = 50)
    last_name = models.CharField(max_length=50)
    email_id = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=50)
